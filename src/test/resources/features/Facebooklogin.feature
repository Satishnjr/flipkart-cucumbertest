Feature: Testing the fb application
   
   Scenario: Verify the user is able to login to fb succesfully
    Given the user navigates to fb page
    When the user enters "<username>" and "<password>" and clicks on login button
    Then the user is able to login successfully and will be navigated to fb home page
    Then the user is able to logout successfully
    
   