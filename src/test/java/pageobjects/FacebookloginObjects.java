package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import helpers.BaseClass;

public class FacebookloginObjects extends BaseClass {

	public FacebookloginObjects(WebDriver driver) {
		super(driver);
	}

	
	@FindBy(how = How.XPATH, using = "//input[@id='email']")
	public static WebElement username;
	
	@FindBy(how = How.XPATH, using = "//input[@id='pass']")
	public static WebElement password;
	
	@FindBy(how = How.XPATH, using = "//input[@id='u_0_2']")
	public static WebElement login_btn;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(), 'John Keerthana')]")
	public static WebElement verify_unametext;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(), 'Account Settings')]")
	public static WebElement logout_dd;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='_54nh'])[9]")
	public static WebElement logout;
	
	
}


       