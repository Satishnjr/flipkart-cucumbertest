package stepdefinitions;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.Hooks;
import pageobjects.FacebookloginObjects;

public class Facebookloginsteps {
	public WebDriver driver;

	public Facebookloginsteps() {
		driver = Hooks.driver;
		PageFactory.initElements(driver, FacebookloginObjects.class);

	}

	@Given("^the user navigates to fb page$")
	public void the_user_navigates_to_fb_page() throws Throwable {
		driver.get("https://www.facebook.com/");
	}

	@When("^the user enters \"([^\"]*)\" and \"([^\"]*)\" and clicks on login button$")
	public void the_user_enters_and_and_clicks_on_login_button(String arg1, String arg2) throws Throwable {
		FacebookloginObjects.username.sendKeys("8309713319");
		FacebookloginObjects.password.sendKeys("9666010667");
		FacebookloginObjects.login_btn.click();
				
	}

	@Then("^the user is able to login successfully and will be navigated to fb home page$")
	public void the_user_is_able_to_login_successfully_and_will_be_navigated_to_fb_home_page() throws Throwable {
		String text = FacebookloginObjects.verify_unametext.getText();
		System.out.println(text);
		Assert.assertEquals(text, "John Keerthana");
	}

	@Then("^the user is able to logout successfully$")
	public void the_user_is_able_to_logout_successfully() throws Throwable {
		FacebookloginObjects.logout_dd.click();
		FacebookloginObjects.logout.click();

	}
}