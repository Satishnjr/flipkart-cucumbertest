package helpers;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features/Facebooklogin.feature", plugin = { "pretty", "html:target/cucumber-html-report",
		"json:cucumber.json", "rerun:target/rerun.txt" }, tags = {}, glue = { "helpers", "stepdefinitions" })
public class TestRunner {

}
