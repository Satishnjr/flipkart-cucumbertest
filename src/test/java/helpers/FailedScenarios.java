package helpers;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, features = "@target/rerun.txt", // Cucumber picks the failed scenarios from this file
		plugin = { "pretty", "html:target/cucumber-html-report", "json:cucumber.json"}, glue = { "helpers", "stepdefinitions"  })

public class FailedScenarios {

}
